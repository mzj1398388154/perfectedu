import React from 'react';
import './App.css';
import Contact from './components/Contact'

function App() {
  return (
    <div className="App">
      <div id="cspio-page">
        <div id="cspio-content">
          <h1 id="cspio-headline">Perfect Education</h1>
          <div id="cspio-description" style={{opacity:.7}}>International education with high-quality teaching resource sharing.</div>
            <div id="cspio-description">Hi, we are working on our website design. Please kindly leave your details below, and our adminstrators will contact you shortly.</div>
            <Contact />
        </div>
      </div>
    </div>
  );
}

export default App;
