import React from 'react';
import emailjs from 'emailjs-com';

export default function ContactUs() {
  const style = {
    input:{margin:'10px'}
  }

  function sendEmail(e) {
    e.preventDefault();

    emailjs.sendForm('contactform', 'template_z24rgxy', e.target, 'user_pckaLaJV5Y4u3PX5Zqq4Z')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });
      e.target.reset();
    alert('Your form has been submitted successfully! We will get back to you as soon as possible.')  
  }

  return(
  <form className="contact-form" onSubmit={sendEmail}>
      <div id="cspio-field-wrapper">
        <div class="row" style={style.input}>
          <div class="col-md-12 seperate"><div class="input-group"><input type="text" placeholder = "Your Name" name="name" class="form-control input-lg form-el"/></div></div>
        </div>
        <div class="row" style={style.input}>
          <div class="col-md-12 seperate"><div class="input-group"><input type="text" placeholder = "Your Email" name="email" class="form-control input-lg form-el"/></div></div>
        </div>
        <div class="row"><span class="input-group-btn"><input type="submit" value="Send" class="btn btn-lg btn-primary form-el noglow"/></span></div>
      </div>
  </form>
  );
}
